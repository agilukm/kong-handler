<?php

namespace Spada\KongHandler\Facades;

use Spada\KongHandler\Contracts\AbstractConsumer;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Illuminate\Routing\Router
 */
class Kong extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'kong';
    }
}
