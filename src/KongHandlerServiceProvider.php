<?php

namespace Spada\KongHandler;

use Illuminate\Support\ServiceProvider as BaseProvider;
use Spada\KongHandler\Contracts\AbstractConsumer;
use Spada\KongHandler\Middleware\KongHandlerMiddleware;
use Spada\KongHandler\Facades\Kong;
use Spada\KongHandler\Models\User;
use Spada\KongHandler\Models\ServiceAccount;
use Spada\KongHandler\Models\ServiceAccountConsumer;
use Exception;

class KongHandlerServiceProvider extends BaseProvider
{
    public function boot()
    {
        // laravel compability, ref: https://github.com/laravel/lumen-framework/issues/113
        if (isset($this->app['router'])) {
            $this->app['router']->middleware([
                'auth' => KongHandlerMiddleware::class,
            ]);
        } else {
            $this->app->routeMiddleware([
                'auth' => KongHandlerMiddleware::class,
            ]);
        }

        $this->app['auth']->viaRequest('api', function ($request) {
            try {
                $kong = $this->app['kong'];
                if ($kong->getToken()) {
                    return new User([
                        'key' => $kong->getUserId(),
                        'id' => $kong->getUserId(),
                        'username' => $kong->getUserInfo()->username,
                        'password_status' => $kong->getUserInfo()->password_status,
                        'name' => $kong->getUserInfo()->name,
                        'level' => $kong->getUserInfo()->level,
                        'lptk_id' => $kong->getUserInfo()->lptk_id,
                        'is_active' => $kong->getUserInfo()->is_active,
                        'created_at' => $kong->getUserInfo()->created_at,
                        'token' => $kong->getToken(),
                        'isService' => $kong->isService(),
                        'timezone' => 'UTC',
                        'student_id' => $kong->getUserInfo()->student_id,
                        'lecturer_id' => $kong->getUserInfo()->lecturer_id,
                        'student_ids' => $kong->getUserInfo()->student_ids,
                        'code' => $kong->getUserInfo()->code,
                        'thumbnail' => $kong->getUserInfo()->thumbnail
                    ]);
                }

                return new User(['timezone' => 'UTC']);
            } catch (Exception $e) {
                return null;
            }
        });
    }

    public function register()
    {
        $this->app->bind('kong', function ($app) {
            return ConsumerFactory::build($app['request']);
        });
    }
}
