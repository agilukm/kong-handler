<?php

namespace Spada\KongHandler\Exceptions;

class ConsumerNotFoundException extends \RuntimeException {}
