<?php

namespace Spada\KongHandler\Exceptions;

class MethodNotFoundException extends \RuntimeException {}
