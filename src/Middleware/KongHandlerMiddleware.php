<?php

namespace Spada\KongHandler\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class KongHandlerMiddleware
{
    protected $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest() || !$this->auth->user()->isValid()) {
            //handle internal service request
            if ($this->isInternalService($request)) {
                return $next($request);
            };
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        
        return $next($request);
    }

    private function isInternalService($request)
    {
        $internal = $request->header('Internal');
        return $internal=="true";
    }
}
