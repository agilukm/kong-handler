<?php

namespace Spada\KongHandler\Models;

use Spada\KongHandler\Contracts\AbstractActor;

class User extends AbstractActor
{
	public function getKey()
	{
		return (int) $this->attributes['key'];
	}

	public function getInfo()
	{
		return $this->attributes['info'];
	}

	public function isValid()
	{
		return !empty($this->getKey());
	}
}
