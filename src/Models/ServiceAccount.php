<?php

namespace Spada\KongHandler\Models;

use Spada\KongHandler\Contracts\AbstractActor;

class ServiceAccount extends AbstractActor
{

    public function getKey()
    {
        return (int) $this->attributes['key'];
    }

    public function getInfo()
    {
        return $this->attributes['info'];
    }

    public function isValid()
    {
        return !empty($this->getKey());
    }

    public function getConsumer()
    {
        return $this->attributes['consumer'];
    }

    public function getAudience()
    {
        return $this->attributes['audience'];
    }

}
