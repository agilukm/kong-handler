<?php

namespace Spada\KongHandler\Models;

use Spada\KongHandler\Contracts\AbstractConsumer;
use Spada\KongHandler\Exceptions\MethodNotFoundException;

class AnonymousConsumer extends AbstractConsumer
{
	public function check()
	{
		throw new MethodNotFoundException("Method check not found", 1);
	}
}
