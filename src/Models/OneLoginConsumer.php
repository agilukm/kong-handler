<?php

namespace Spada\KongHandler\Models;

use Spada\KongHandler\Contracts\AbstractConsumer;

class OneLoginConsumer extends AbstractConsumer
{
	public $userInfo;
	public $kwuid;

	public function setUserInfo(UserInfo $userInfo)
	{
		$this->userInfo = $userInfo;
		return $this;
	}

	public function getUserInfo()
	{
		return $this->userInfo;
	}

	public function setUserId($kwuid)
	{
		$this->kwuid = $kwuid;
		return $this;
	}

	public function getUserId()
	{
		return $this->kwuid;
	}

	public function check()
	{
		return !empty($this->userId);
	}
}
