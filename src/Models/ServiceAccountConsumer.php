<?php

namespace Spada\KongHandler\Models;

use Spada\KongHandler\Contracts\AbstractConsumer;

class ServiceAccountConsumer extends AbstractConsumer
{
    public $userInfo;
	public $userId;
    public $audience;
    public $appId;

	public function setUserInfo(UserInfo $userInfo)
	{
		$this->userInfo = $userInfo;
		return $this;
	}

	public function getUserInfo()
	{
		return $this->userInfo;
	}

	public function setUserId($userId)
	{
		$this->userId = $userId;
		return $this;
	}

	public function getUserId()
	{
		return $this->userId;
	}

    public function parseSubject($subject)
    {
        if (trim($subject) == "") {
            return $this;
        }
        $userInfo = [
        ];
        parse_str(str_replace(";", "&", $subject), $userInfo);
        $this->setUserInfo(new UserInfo($userInfo));
        
        return $this;
    }

    public function setAudience($audience)
    {
        $this->audience = $audience;
        return $this;
	}
	
    public function setAppId($appId)
    {
        $this->appId = $appId;
        return $this;
    }

    public function getAudience()
    {
        return $this->audience;
    }


    public function getAppId()
    {
        return $this->appId;
    }


	public function check()
	{
		return !empty($this->userId);
	}
}
