<?php

namespace Spada\KongHandler\Models;

class UserInfo
{
	public $updatedAt;
	public $email;
	public $name;
	public $username;
	public $password_status;
	public $level;
	public $lptk_id;
	public $id;
	public $is_active;
	public $created_at;

	public function __construct($attributes = [])
	{
		$this->id = isset($attributes['id']) ? $attributes['id'] : null;
		$this->email = isset($attributes['email']) ? $attributes['email'] : null;
		$this->username = isset($attributes['username']) ? $attributes['username'] : null;
		$this->password_status = isset($attributes['password_status']) ? $attributes['password_status'] : null;
		$this->name = isset($attributes['name']) ? $attributes['name'] : null;
		$this->level = isset($attributes['level']) ? $attributes['level'] : null;
		$this->lptk_id = isset($attributes['lptk_id']) ? $attributes['lptk_id'] : null;
		$this->is_active = isset($attributes['is_active']) ? $attributes['is_active'] : null;
		$this->created_at = isset($attributes['created_at']) ? $attributes['created_at'] : null;
		$this->updatedAt = isset($attributes['updated_at']) ? $attributes['updated_at'] : null;
		$this->student_id = isset($attributes['student_id']) ? $attributes['student_id'] : null;
		$this->lecturer_id = isset($attributes['lecturer_id']) ? $attributes['lecturer_id'] : null;
		$this->code = isset($attributes['code']) ? $attributes['code'] : 1;
		$this->thumbnail = isset($attributes['thumbnail']) ? $attributes['thumbnail'] : 1;
		$this->student_ids = isset($attributes['student_ids']) ? $attributes['student_ids'] : null;
		// Use name as given name and family name
		if (trim($this->name) != "") {
			$slicedName = explode(" ", $this->name);
		}
	}

	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;
		return $this;
	}

	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}

	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}
}
