<?php

namespace Spada\KongHandler;

use Illuminate\Http\Request;
use Spada\KongHandler\Models\ServiceAccountConsumer;
use Spada\KongHandler\Models\AnonymousConsumer;
use Spada\KongHandler\Models\UserInfo;
use Spada\KongHandler\Exceptions\ConsumerNotFoundException;
use Spada\KongHandler\Models\OneLoginConsumer;

class ConsumerFactory
{
    public static function build(Request $request)
    {
        if (!$request->hasHeader('Authorization')) {
            throw new ConsumerNotFoundException();
        }

        // check if anonymous
        if ($request->hasHeader('x-anonymous-consumer')) {
            return new AnonymousConsumer;
        }

        $isService = !$request->hasHeader('x-userinfo');
        if (!$isService) {
            $userInfo = json_decode(stripslashes($request->header('x-userinfo')), true);
            $consumer = new OneLoginConsumer;

            return $consumer->setToken($request->header('authorization'))
                ->setIsService($isService)
                ->setUserInfo(new UserInfo($userInfo))
                ->setUsername($request->header('x-consumer-username'))
                ->setUserId($userInfo['id']);
        }

        $consumer = new ServiceAccountConsumer;
        $consumer->setToken($request->header('authorization'))
            ->setUsername($request->header('x-consumer-username'))
            ->setUserId($request->header('x-consumer-token-user-id'))
            ->parseSubject($request->header('x-consumer-token-user-email'))
            ->setIsService($isService);

        return $consumer;

    }
}
