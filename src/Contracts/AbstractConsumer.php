<?php

namespace Spada\KongHandler\Contracts;

use Spada\KongHandler\Traits;

abstract class AbstractConsumer
{
	protected $token;
	protected $isService;
	protected $username;


	public function setToken($token)
	{
		$this->token = $token;
		return $this;
	}

	public function getToken()
	{
		return $this->token;
	}

	public function setIsService($isService)
	{
		$this->isService = $isService;
		return $this;
	}

	public function isService()
	{
		return $this->isService;
	}

	public function setUsername($username)
	{
		$this->username = $username;
		return $this;
	}

	public function getUsername()
	{
		return $this->username;
	}

}
