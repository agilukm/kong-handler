<?php

namespace Spada\KongHandler\Contracts;

use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Access\Authorizable;
use Spada\KongHandler\Traits\AuthorizableTrait;

abstract class AbstractActor extends GenericUser implements Authorizable
{
	use AuthorizableTrait;

	public function getTimezone()
	{
		return $this->attributes['timezone'];
	}

	public function getToken()
	{
		$token = $this->attributes['token'];
		$token = preg_replace('/^' . preg_quote('Bearer', '/') . '/', '', $token);
		$token = preg_replace('/^' . preg_quote('bearer', '/') . '/', '', $token);
		return trim($token);
	}

	public function isService()
	{
		return $this->attributes['isService'];
	}

	abstract public function isValid();
}
