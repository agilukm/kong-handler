# Kong Handler

Package that used for process Kong's header and overriding underlying Auth mechanism

### Installation

Install with composer
```
composer require spada/kong-handler
```

### Usage

Register `KongServiceProvider`. Make sure you don't register any AuthServiceProvider.

```php
use Spada\KongHandler\KongHandlerServiceProvider;

return [
    'providers' => [
    	// 
    	// other service providers
    	//
        KongHandlerServiceProvider::class
    ]
];

```


Now, we can use Auth like usual.

```php
// get kwuid
$kwuid = Auth::user()->getKey();

// get user access token
$token = Auth::user()->getToken();

// check if authenticated
$authenticated = Auth::check();

// get user info
// get name
$email = Auth::user()->getInfo()->getName();

// get email
$email = Auth::user()->getInfo()->getEmail();

```

Added methods for handling service account

```
// check if its service account
Auth::user()->isService();

// get service account consumer
Auth::user()->getConsumer();

// get service account audience
Auth::user()->getAudience();
```


### Tests

For **LOCAL** smoked test, you need to provide some headers below

| Headers | OneLogin Consumer | Service Account Consumer| Value |
| ------- | ----------------- | ----------------------- | ----- |
| Authorization | :heavy_check_mark: | :heavy_check_mark: | *example:* `Bearer abcd` |
| x-userinfo | :heavy_check_mark: | :heavy_multiplication_x: | *example:* `{\"given_name\":\"kelle1\",\"updated_at\":\"2018-01-23T07:19:36.950Z\",\"family_name\":\"kelle1\",\"preferred_username\":\"kelle1\",\"custom_fields\":{\"KW_UID\":\"556396\",\"KW_OID\":\"101\"},\"email\":\"kelle1@kw.com\",\"name\":\"kelle1 kelle1\",\"sub\":\"37087183\"}` |
| Content-Type | :heavy_check_mark: | :heavy_check_mark: | `application/json` |
| x-consumer-username | :heavy_check_mark: | :heavy_check_mark: | *example:* `onelogin` |

#### Sample header for OneLogin

```
accept: application/vnd.api+json
authorization: Bearer ODIyZmQ4MjctZThmYy00Nzc1LWJiNDAtNGIxYTA5YTZmNGU25pyUpO04RVRCsV9R-cmOwOZuKefUlRLBPYYkwOzQayVSfBf57tankKkY8DMXAwKdlYHYTKZX190j9I35kGr_kg
cache-control: no-cache
content-type: application/vnd.api+json
x-consumer-username: onelogin
x-userinfo: {\"given_name\":\"kelle1\",\"updated_at\":\"2018-01-23T07:19:36.950Z\",\"family_name\":\"kelle1\",\"preferred_username\":\"kelle1\",\"custom_fields\":{\"KW_UID\":\"556396\",\"KW_OID\":\"101\"},\"email\":\"kelle1@kw.com\",\"name\":\"kelle1 kelle1\",\"sub\":\"37087183\"}
```

#### Sample header for service account

```
x-consumer-aid:testing
x-consumer-token-audience:https://dev-kong.command-api.kw.com
x-consumer-token-user-id:37849
x-consumer-token-user-email:name=Gail Anderson;email=gandersoniw@yahoo.com
x-consumer-username:testing
x-consumer-id:9bdf5a6e-775b-434d-9922-9987524e712d
authorization:Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhaWQiOiJ0ZXN0aW5nIiwiYXVkIjoiaHR0cHM6Ly9kZXYta29uZy5jb21tYW5kLWFwaS5rdy5jb20iLCJleHAiOjE2MDMxMjEzMjYsImlhdCI6MTUyODQyOTQ1NSwiaXNzIjoiZ0NWbVZJbFVBc0dpSzJxd2tZMXpveFRRRVBHdU9ERDAiLCJuYmYiOjE1Mjg0Mjk0NTUsInN1YiI6Im5hbWU9R2FpbCBBbmRlcnNvbjtlbWFpbD1nYW5kZXJzb25pd0B5YWhvby5jb20iLCJ1aWQiOiIzNzg0OSJ9.ci1-EFSraTWV06-6v_NN7QBcK5Bs6DgqruMMTpQRJ_U
content-type:application/vnd.api+json
```

### Other

For more information about how Kong Consumer works, please read https://github.com/KWRI/aOS/wiki/Misc-:-Kong-Consumer-and-OIDC-Plugin
